FROM node:18 as nodebuilder
RUN mkdir publish
COPY ./inventory.dashboard/Client publish/Client/
COPY ./inventory.dashboard/wwwroot publish/wwwroot/
WORKDIR /publish
RUN cd ./Client && npm install && npm run build

FROM mcr.microsoft.com/dotnet/sdk:7.0 AS build-env
WORKDIR /App

# Copy everything

COPY ./inventory.services inventory.services/
COPY ./inventory.dashboard inventory.dashboard/
# Restore as distinct layers
RUN dotnet restore inventory.dashboard/inventory.dashboard.csproj
# Build and publish a release
RUN dotnet publish inventory.dashboard/inventory.dashboard.csproj -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:7.0
WORKDIR /App
COPY --from=build-env /App/out .
COPY --from=nodebuilder publish/wwwroot /App/wwwroot/
ENTRYPOINT ["dotnet", "inventory.dashboard.dll"]