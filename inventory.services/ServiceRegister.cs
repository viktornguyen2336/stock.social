﻿using inventory.services.MongoDb;
using inventory.services.MongoDb.Collection.Storage;
using inventory.services.MongoDb.Factory;
using inventory.services.MongoDb.Repository;
using inventory.services.Services;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;

namespace inventory.services
{
    public static class ServiceRegister
    {
        public static MongoClient RegisterMongoDb(this IServiceCollection serviceCollection, MongoDbSetting setting)
        {
            var settings = MongoClientSettings.FromConnectionString(setting.ConnectionString);
            serviceCollection.AddSingleton(setting);
            var mongoClient = new MongoClient(settings);
            serviceCollection.AddSingleton(mongoClient);
            serviceCollection.AddSingleton<IMongoClientFactory, MongoClientFactory>();
            serviceCollection.AddScoped(typeof(IMongoRepository<>), typeof(MongoRepositoryBase<>));
            serviceCollection.AddScoped<IStaticPostsInfoService, StaticPostInfoService>();
            serviceCollection.AddScoped<IS3Service, S3Service>();
            return mongoClient;
        }
    }
}