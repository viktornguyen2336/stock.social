﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;

namespace inventory.services.Services;

public interface IS3Service
{
    Task<string> PushImageToS3(Stream stream, string fielName, string folder = "");
    Task<string> PushImageToS3WithExpired(Stream stream, string fielName, string folder = "");
    Task RemoveExpiredForImage(string filePath);
}

public class S3Service : IS3Service
{
    public async Task<string> PushImageToS3(Stream stream, string fielName, string folder = "")
    {
        var awsCredentials =
            new Amazon.Runtime.BasicAWSCredentials("AKIAYHK4HVA3YSLQUONH", "iCtk8Q/QoO+IxLkKNkyR8D3Q0FqFkH5rxJ4+gaQf");
        var s3Client = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.APSoutheast1);
        var bucketName = "static-data-stock";
        var extensionImage = Path.GetExtension(fielName);
        if (string.IsNullOrEmpty(extensionImage))
        {
            return null;
        }

        var randomName = Path.ChangeExtension(
            Path.GetRandomFileName(),
            extensionImage
        );
        var key = randomName;
        if (!string.IsNullOrEmpty(folder))
        {
            key = $"{folder}/{key}";
        }

        var request = new PutObjectRequest
        {
            BucketName = bucketName,
            Key = key,
            InputStream = stream,
            // TagSet = tags
        };

        // Thực hiện upload file lên S3
        await s3Client.PutObjectAsync(request);
        return $"https://{bucketName}.s3.ap-southeast-1.amazonaws.com/{key}";
    }

    public async Task<string> PushImageToS3WithExpired(Stream stream, string fielName, string folder = "")
    {
        var awsCredentials =
            new Amazon.Runtime.BasicAWSCredentials("AKIAYHK4HVA3YSLQUONH", "iCtk8Q/QoO+IxLkKNkyR8D3Q0FqFkH5rxJ4+gaQf");
        var s3Client = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.APSoutheast1);
        var bucketName = "static-data-stock";
        var extensionImage = Path.GetExtension(fielName);
        if (string.IsNullOrEmpty(extensionImage))
        {
            return null;
        }

        var randomName = Path.ChangeExtension(
            Path.GetRandomFileName(),
            extensionImage
        );
        var key = randomName;
        if (!string.IsNullOrEmpty(folder))
        {
            key = $"{folder}/{key}";
        }

        var tags = new List<Tag>
        {
            new Tag { Key = "temp", Value = "Value1" },
        };

        var request = new PutObjectRequest
        {
            BucketName = bucketName,
            Key = key,
            InputStream = stream,
            TagSet = tags
        };

        // Thực hiện upload file lên S3
        await s3Client.PutObjectAsync(request);
        return $"https://{bucketName}.s3.ap-southeast-1.amazonaws.com/{key}";
    }

    public async Task RemoveExpiredForImage(string filePath)
    {
        var awsCredentials =
            new Amazon.Runtime.BasicAWSCredentials("AKIAYHK4HVA3YSLQUONH", "iCtk8Q/QoO+IxLkKNkyR8D3Q0FqFkH5rxJ4+gaQf");
        var s3Client = new AmazonS3Client(awsCredentials, Amazon.RegionEndpoint.APSoutheast1);
        var bucketName = "static-data-stock";
        var basePath = $"https://{bucketName}.s3.ap-southeast-1.amazonaws.com/";
        var key = filePath.Replace(basePath, "");
        var getObjectTaggingRequest = new GetObjectTaggingRequest
        {
            BucketName = bucketName,
            Key = key
        };
        var tagSet = await s3Client.GetObjectTaggingAsync(getObjectTaggingRequest);

        // Loại bỏ tag cụ thể
        tagSet.Tagging.RemoveAll(tag => tag.Key == "temp");

        // Tạo yêu cầu để đặt lại tag của đối tượng
        var putTaggingRequest = new PutObjectTaggingRequest
        {
            BucketName = bucketName,
            Key =key,
            Tagging = new Tagging()
            {
                TagSet = tagSet.Tagging
            }
        };

        await s3Client.PutObjectTaggingAsync(putTaggingRequest);
    }
}