﻿using AutoMapper;
using inventory.services.Model;
using inventory.services.Model.Posts;
using inventory.services.MongoDb.Collection.Storage;
using inventory.services.MongoDb.Common;
using inventory.services.MongoDb.Repository;
using MongoDB.Driver.GeoJsonObjectModel;

namespace inventory.services.Services
{
    public interface IStaticPostsInfoService
    {
        Task<GetPostsResponse> GetStaticPost(int pageIndex, int pageSize);

        Task<dynamic> GetpostByName(string postName, int pageIndex, int pageSize, string languageCode);

        Task UpdatePost(PostDetail postStaticInfo);

        Task InsertPost(PostDetail postStaticInfo);

        Task<PostDetailResource> GetPostById(string id);
    }

    public class StaticPostInfoService : IStaticPostsInfoService
    {
        private readonly IMongoRepository<Post> _postRepo;
        public StaticPostInfoService(IMongoRepository<Post> postRepo)
        {
            _postRepo = postRepo;
        }
        public async Task<PostDetailResource> GetPostById(string id)
        {
            var post = await _postRepo.GetAsync(x => x.Id == id.ToString());

            if (post != null)
            {
                var mapperConfigurationonfig = new MapperConfiguration(cfg => { cfg.AddProfile(new MappingProfile()); });
                var mapper = mapperConfigurationonfig.CreateMapper();
                var postDetail = mapper.Map<Post, PostDetailResource>(post.FirstOrDefault());
                return postDetail;
            }

            return new PostDetailResource();
        }

        public Task<dynamic> GetpostByName(string postName, int pageIndex, int pageSize, string languageCode)
        {
            throw new NotImplementedException();
        }

        public async Task InsertPost(PostDetail postStaticInfo)
        {
            var postDetail = FormatpostDataBeforeUpdateDB(postStaticInfo);
            postDetail.CreatedAt = DateTime.UtcNow;
            await _postRepo.AddAsync(postDetail);
        }

        public async Task UpdatePost(PostDetail postStaticInfo)
        {
            var postDetail = FormatpostDataBeforeUpdateDB(postStaticInfo);
            postDetail.UpdatedAt = DateTime.UtcNow;
            await _postRepo.UpdateAsync(postDetail.Id, postDetail);
        }
        public Post FormatpostDataBeforeUpdateDB(PostDetail postDetail)
        {
            var mapperConfigurationonfig = new MapperConfiguration(cfg => { cfg.AddProfile(new MappingProfile()); });
            var mapper = mapperConfigurationonfig.CreateMapper();
            var postStaticInfo = mapper.Map<PostDetail, Post>(postDetail);
            return postStaticInfo;
        }
        public async Task<GetPostsResponse> GetStaticPost(int pageIndex, int pageSize)
        {
            var posts = await _postRepo.GetAsync(x => true,
                new Option<Post> { Skip = (pageIndex - 1) * pageSize, Limit = pageSize });

            if (posts == null)
                return new GetPostsResponse() { PostStaticInfo = new List<PostDetailResource>(), TotalPosts = 0 };

            var mapperConfigurationonfig = new MapperConfiguration(cfg => { cfg.AddProfile(new MappingProfile()); });
            var mapper = mapperConfigurationonfig.CreateMapper();

            var postDetails = posts.ToList().Select(post => mapper.Map<Post, PostDetailResource>(post));

            var total = await _postRepo.CountAsync(c => true);

            return new GetPostsResponse() { PostStaticInfo = postDetails, TotalPosts = total };
        }
    }
}
