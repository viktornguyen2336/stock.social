using AutoMapper;
using inventory.services.Model.Posts;
using inventory.services.MongoDb.Collection.Storage;

namespace inventory.services.Model
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Post, PostDetail>();
            CreateMap<PostDetail, Post>();
            CreateMap<Post, PostDetailResource>();
            CreateMap<PostDetailResource, Post>();
        }
    }
}