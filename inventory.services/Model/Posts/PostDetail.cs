﻿namespace inventory.services.Model.Posts
{
    public class PostDetail
    {
        public string Title { get; set; }
        
        public string Banner { get; set; }
        
        public string Content { get; set; }
        
        public string PostType { get; set; }
        
        public string OwnerId { get; set; }
        
        public string EnterPriseId { get; set; }
        
        public string CommentIds { get; set; }
        
    }
}
