using inventory.services.MongoDb.Collection.Storage;

namespace inventory.services.Model;

public class PostsResponse
{
    public IEnumerable<Post> PoststaticInfo;

    public long TotalPosts;
}