﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inventory.services.Model.Posts
{
    public class PostDetailResource
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Banner { get; set; }
        public string Content { get; set; }
        public string PostType { get; set; }
        public string OwnerId { get; set; }
        public string EnterPriseId { get; set; }
        public string CommentIds { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public DateTime? UpdatedAt { get; set; }
    }
}
