namespace inventory.services.Model.Posts
{
    public class GetPostRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}