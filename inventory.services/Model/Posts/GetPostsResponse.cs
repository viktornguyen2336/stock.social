using inventory.services.Model.Posts;

namespace inventory.services.Model.Posts
{
    public class GetPostsResponse
    {
        public IEnumerable<PostDetailResource> PostStaticInfo { get; set; }

        public long TotalPosts { get; set; }
    }
}