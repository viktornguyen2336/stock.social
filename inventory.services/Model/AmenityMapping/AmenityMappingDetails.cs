namespace inventory.services.Model.AmenityMapping
{
    public class AmenityMappingDetail
    {
        public string Id { get; set; }
        
        public string SourceCode { get; set; }

        public string SourceProviderName { get; set; }

        public string DestinationCode { get; set; }

        public string DestinationProviderName { get; set; }
    }
}