namespace inventory.dashboard.Model
{
    public class MappingRequest
    {
        public int PageIndex { get; set; }

        public int PageSize { get; set; }

        public string Name { get; set; }

        public long HotelId { get; set; }
    }
}