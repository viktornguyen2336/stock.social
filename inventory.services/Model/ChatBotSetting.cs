﻿namespace inventory.services.Model;

public class ChatBotSetting
{
    public string SecretKey { get; set; }

    public string Url { get; set; }

    public string SessionId { get; set; }
}