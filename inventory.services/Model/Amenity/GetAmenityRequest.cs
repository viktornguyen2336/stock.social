namespace inventory.services.Model.Amenity;

public class GetAmenityRequest
{
    public int PageIndex { get; set; }

    public int PageSize { get; set; }

    public string LanguageCode { get; set; }
}