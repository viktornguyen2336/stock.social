﻿using MongoDB.Driver;

namespace inventory.services.MongoDb.Factory
{
    public interface IMongoClientFactory
    {
        MongoClient GetMongoClient();
    }

    public class MongoClientFactory : IMongoClientFactory
    {
        private readonly MongoClient _mongoClient;

        public MongoClientFactory(MongoClient mongoClient)
        {
            _mongoClient = mongoClient;
        }

        public MongoClient GetMongoClient()
        {
            return _mongoClient;
        }
    }
}