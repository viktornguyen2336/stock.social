﻿using MongoDB.Driver;

namespace inventory.services.MongoDb.Common
{
    internal static class MongoExtension
    {
        public static IMongoCollection<TEntity> GetCollectionFromAttribute<TEntity>(this IMongoDatabase mongoDb)
        {
            var typeEntity = typeof(TEntity);

            string collectioName = typeEntity.Name;

            var collectionAttribute = (CollectionNameAttribute)Attribute.GetCustomAttribute(typeEntity, typeof(CollectionNameAttribute));
            if (collectionAttribute != null)
            {
                collectioName = collectionAttribute.Name;
            }

            return mongoDb.GetCollection<TEntity>(collectioName);
        }
    }
}