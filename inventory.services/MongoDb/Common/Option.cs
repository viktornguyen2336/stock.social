﻿namespace inventory.services.MongoDb.Common
{
    public class Option<TSource>
    {
        public int? Skip { get; set; }
        public int? Limit { get; set; }
    }
}