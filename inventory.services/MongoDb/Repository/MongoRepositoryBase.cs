﻿using inventory.services.MongoDb.Common;
using inventory.services.MongoDb.Factory;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Linq.Expressions;
using CollectionBase = inventory.services.MongoDb.Collection.CollectionBase;

namespace inventory.services.MongoDb.Repository
{
    public interface IMongoRepository<TEntity>
    {
        Task<long> CountAsync(Expression<Func<TEntity, bool>> predicate = null);

        Task<IEnumerable<TProjection>> GetAsync<TProjection>(Expression<Func<TEntity, bool>> condition, Expression<Func<TEntity, TProjection>> projection, Option<TEntity> option = null);

        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> condition, Option<TEntity> option = null);

        Task<TEntity> GetOneAsync(Expression<Func<TEntity, bool>> filterExpression);

        Task<TProjection> GetOneAsync<TProjection>(Expression<Func<TEntity, bool>> filterExpression, Expression<Func<TEntity, TProjection>> projection);

        Task<TEntity> GetByIdAsync(string id);

        Task AddAsync(TEntity entity);

        Task AddManyAsync(IEnumerable<TEntity> entities);

        Task UpdateAsync(string id, TEntity entity);

        Task UpdateOneAsync(string id, params UpdateFieldInfo[] updateFields);

        Task UpdateOneAsync(FilterDefinition<TEntity> filter, params UpdateFieldInfo[] updateFields);

        Task DeleteAsync(string id);

        Task<List<TProject>> AggregateAsync<TProject>(IEnumerable<BsonDocument> pipelinesDocument);

        IMongoCollection<TEntity> Collection { get; }

    }

    public class MongoRepositoryBase<TEntity> : IMongoRepository<TEntity> where TEntity : CollectionBase
    {
        protected readonly MongoClient _mongoClient;
        protected readonly IMongoCollection<TEntity> _collection;

        public IMongoCollection<TEntity> Collection { get => _collection; }

        public MongoRepositoryBase(IMongoClientFactory mongoClientFactory, MongoDbSetting mongoDbSetting)
        {
            _mongoClient = mongoClientFactory.GetMongoClient();
            _collection = _mongoClient.GetDatabase(mongoDbSetting.Database).GetCollectionFromAttribute<TEntity>();
        }

        public async Task<long> CountAsync(Expression<Func<TEntity, bool>> predicate = null)
        {
            var queryable = _collection.AsQueryable();
            return await queryable.LongCountAsync(predicate);
        }

        public async Task<IEnumerable<TProjection>> GetAsync<TProjection>(Expression<Func<TEntity, bool>> condition, Expression<Func<TEntity, TProjection>> projection, Option<TEntity> option = null)
        {
            var queryable = _collection.AsQueryable();
            var skip = 0;
            var limit = 0;

            if (option != null)
            {
                if (option.Skip.HasValue && option.Skip.Value > 0)
                {
                    skip = option.Skip.Value;
                }

                if (option.Limit.HasValue && option.Limit.Value > 0)
                {
                    limit = option.Limit.Value;
                }
            }

            if (condition != null)
            {
                queryable = queryable.Where(condition);
            }

            if (skip > 0)
            {
                queryable = queryable.Skip(skip);
            }

            if (limit > 0)
            {
                queryable = queryable.Take(limit);
            }

            return await queryable.Select(projection).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> condition, Option<TEntity> option = null)
        {
            var queryable = _collection.AsQueryable();

            var skip = 0;
            var limit = 0;

            if (option != null)
            {
                if (option.Skip.HasValue && option.Skip.Value > 0)
                {
                    skip = option.Skip.Value;
                }

                if (option.Limit.HasValue && option.Limit.Value > 0)
                {
                    limit = option.Limit.Value;
                }
            }

            if (condition != null)
            {
                queryable = queryable.Where(condition);
            }

            if (skip > 0)
            {
                queryable = queryable.Skip(skip);
            }

            if (limit > 0)
            {
                queryable = queryable.Take(limit);
            }

            return await queryable.ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(string id)
        {
            return await _collection.Find<TEntity>(s => s.Id.Equals(id)).FirstOrDefaultAsync();
        }

        public async Task<TEntity> GetOneAsync(Expression<Func<TEntity, bool>> filterExpression)
        {
            var queryable = _collection.AsQueryable();
            if (filterExpression != null)
            {
                queryable = queryable.Where(filterExpression);
            }

            return await queryable.FirstOrDefaultAsync();
        }

        public async Task<TProjection> GetOneAsync<TProjection>(Expression<Func<TEntity, bool>> filterExpression, Expression<Func<TEntity, TProjection>> projection)
        {
            var queryable = _collection.AsQueryable();
            if (filterExpression != null)
            {
                queryable = queryable.Where(filterExpression);
            }

            return await queryable.Select(projection).FirstOrDefaultAsync();
        }

        public async Task AddAsync(TEntity entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task AddManyAsync(IEnumerable<TEntity> entities)
        {
            await _collection.InsertManyAsync(entities);
        }

        public async Task UpdateAsync(string id, TEntity entity)
        {
            await _collection.ReplaceOneAsync(s => s.Id.Equals(id), entity);
        }

        public async Task DeleteAsync(string id)
        {
            await _collection.DeleteOneAsync(s => s.Id.Equals(id));
        }

        public async Task<List<TProject>> AggregateAsync<TProject>(IEnumerable<BsonDocument> pipelinesDocument)
        {
            PipelineDefinition<TEntity, TProject> pipelines = pipelinesDocument.ToArray();
            var result = await _collection.AggregateAsync(pipelines);
            return result.ToList();
        }

        public async Task UpdateOneAsync(string id, params UpdateFieldInfo[] updateFields)
        {
            var filter = Builders<TEntity>.Filter.Eq("_id", new ObjectId(id));
            await UpdateOneAsync(filter, updateFields);
        }

        public async Task UpdateOneAsync(FilterDefinition<TEntity> filter, params UpdateFieldInfo[] updateFields)
        {
            var fieldsUpdate = new List<UpdateDefinition<TEntity>>();
            var updateBuilder = Builders<TEntity>.Update;

            foreach (var updateField in updateFields)
            {
                switch (updateField.UpdateType)
                {
                    case UpdateType.Set:
                        fieldsUpdate.Add(Builders<TEntity>.Update.Set(updateField.FieldName, updateField.Value));
                        break;

                    case UpdateType.Push:
                        fieldsUpdate.Add(Builders<TEntity>.Update.Push(updateField.FieldName, updateField.Value));
                        break;

                    case UpdateType.AddToSet:
                        fieldsUpdate.Add(Builders<TEntity>.Update.AddToSet(updateField.FieldName, updateField.Value));
                        break;
                    case UpdateType.AddToSetEach:
                        fieldsUpdate.Add(Builders<TEntity>.Update.AddToSetEach(updateField.FieldName, updateField.Value));
                        break;
                }
            }
            var update = updateBuilder.Combine(fieldsUpdate);
            await _collection.UpdateOneAsync(filter, update);
        }
    }

    public class UpdateFieldInfo
    {
        public string FieldName { get; set; }

        public UpdateType UpdateType { get; set; }

        public dynamic Value { get; set; }
    }

    public enum UpdateType
    {
        Set,
        Push,
        AddToSet,
        AddToSetEach
    }
}