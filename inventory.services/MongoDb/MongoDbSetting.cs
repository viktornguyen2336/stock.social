﻿namespace inventory.services.MongoDb
{
    public class MongoDbSetting
    {
        public string ConnectionString { get; set; }

        public string Database { get; set; }

        public string IdentityDatabase { get; set; }

        public Dictionary<string, string> DatabaseDocumentMappings { get; set; }

    }
}