﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDbGenericRepository.Attributes;

namespace inventory.services.MongoDb.Collection.Storage
{
    [CollectionName("enterprise")]
    [BsonIgnoreExtraElements]
    public class Enterprise : CollectionBase
    {
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("code")]
        public string Code { get; set; }

        [BsonElement("image_url")]
        public string ImageUrl { get; set; }

        [BsonElement("sector_id")]
        public string SectorId { get; set; }

        [BsonElement("overview_post_id")]
        public string OverviewPostId { get; set; }
    }
}