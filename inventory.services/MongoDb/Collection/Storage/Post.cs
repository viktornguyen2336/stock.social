﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDbGenericRepository.Attributes;

namespace inventory.services.MongoDb.Collection.Storage
{
    [CollectionName("post")]
    [BsonIgnoreExtraElements]
    public class Post : CollectionBase
    {
        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("image_url")]
        public string ImageUrl { get; set; }

        [BsonElement("content")]
        public string Content { get; set; }

        [BsonElement("post_type")]
        public string PostType { get; set; }

        [BsonElement("owner")]
        public Owner Owner { get; set; }

        [BsonElement("author_id")]
        public string AuthorId { get; set; }

        [BsonElement("comment_ids")]
        public string[] CommentIds { get; set; }
    }

    public class Owner
    {
        [BsonElement("owner_type")]
        public string OwnerType { get; set; }

        [BsonElement("owner_id")]
        public string OwnerId { get; set; }
    }
}