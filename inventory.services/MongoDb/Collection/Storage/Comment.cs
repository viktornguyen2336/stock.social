﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDbGenericRepository.Attributes;

namespace inventory.services.MongoDb.Collection.Storage
{
    [CollectionName("comment")]
    [BsonIgnoreExtraElements]
    public class Comment : CollectionBase
    {
        [BsonElement("content")]
        public string Content { get; set; }

        [BsonElement("owner_id")]
        public string OwnerId { get; set; }

        [BsonElement("children_comment_ids")]
        public string[] ChildrenCommentIds { get; set; }

        [BsonElement("parent_comment_id")]
        public string ParentCommentId { get; set; }
    }
}