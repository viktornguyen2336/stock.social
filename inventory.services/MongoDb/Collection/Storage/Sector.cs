﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDbGenericRepository.Attributes;

namespace inventory.services.MongoDb.Collection.Storage
{
    [CollectionName("sector")]
    [BsonIgnoreExtraElements]
    public class Sector : CollectionBase
    {
        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("code")]
        public string Code { get; set; }

        [BsonElement("image_url")]
        public string ImageUrl { get; set; }

        [BsonElement("overview_post_id")]
        public string OverviewPostId { get; set; }

        [BsonElement("enterprise_ids")]
        public string[] EnterpriseIds { get; set; }
    }
}