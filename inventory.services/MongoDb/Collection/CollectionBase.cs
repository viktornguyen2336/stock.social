﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace inventory.services.MongoDb.Collection
{
    public class CollectionBase
    {
        public CollectionBase()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }

        [BsonId]
        public string Id { get; set; }

        [BsonElement("created_at")]
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        [BsonElement("updated_at")]
        public DateTime? UpdatedAt { get; set; }

        [BsonElement("deleted")]
        public bool Deleted { get; set; }
    }
}