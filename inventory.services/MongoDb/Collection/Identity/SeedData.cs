﻿using AspNetCore.Identity.MongoDbCore.Models;
using Microsoft.AspNetCore.Identity;

namespace inventory.services.MongoDb.Collection.Identity
{
    public class SeedData
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public SeedData(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task SeedAdminAsync()
        {
            // Kiểm tra tài khoản admin đã được tạo hay chưa
            var adminUser = await _userManager.FindByEmailAsync("admin@247technical");

            if (adminUser == null)
            {
                // Tạo một tài khoản admin mới
                var adminEmail = "admin@247technical";
                var adminUserName = "admin";

                adminUser = new ApplicationUser
                {
                    UserName = adminUserName,
                    Email = adminEmail,
                    EmailConfirmed = true,
                    Claims = new List<MongoClaim>
                    {
                        new MongoClaim {
                        Type="Level",
                        Value = "Admin",
                        Issuer="System"
                        }
                    }
                };

                await _userManager.CreateAsync(adminUser, "Admin@123#");
            }

            var adminUser2 = await _userManager.FindByEmailAsync("admin2@247technical");

            if (adminUser2 == null)
            {
                // Tạo một tài khoản admin mới
                var adminEmail = "admin2@247technical";
                var adminUserName = "admin2";

                adminUser2 = new ApplicationUser
                {
                    UserName = adminUserName,
                    Email = adminEmail,
                    EmailConfirmed = true,
                    Claims = new List<MongoClaim>
                    {
                        new MongoClaim {
                        Type="Level",
                        Value = "Admin",
                        Issuer="System"
                        }
                    }
                };

                await _userManager.CreateAsync(adminUser2, "Admin2@225#");
            }

            var adminUser3 = await _userManager.FindByEmailAsync("admin3@247technical");

            if (adminUser3 == null)
            {
                // Tạo một tài khoản admin mới
                var adminEmail = "admin3@247technical";
                var adminUserName = "admin3";

                adminUser3 = new ApplicationUser
                {
                    UserName = adminUserName,
                    Email = adminEmail,
                    EmailConfirmed = true,
                    Claims = new List<MongoClaim>
                    {
                        new MongoClaim {
                        Type="Level",
                        Value = "Admin",
                        Issuer="System"
                        }
                    }
                };

                await _userManager.CreateAsync(adminUser3, "Admin3@991#");
            }

            var adminUser4 = await _userManager.FindByEmailAsync("admin4@247technical");

            if (adminUser4 == null)
            {
                // Tạo một tài khoản admin mới
                var adminEmail = "admin4@247technical";
                var adminUserName = "admin4";

                adminUser4 = new ApplicationUser
                {
                    UserName = adminUserName,
                    Email = adminEmail,
                    EmailConfirmed = true,
                    Claims = new List<MongoClaim>
                    {
                        new MongoClaim {
                        Type="Level",
                        Value = "Admin",
                        Issuer="System"
                        }
                    }
                };

                await _userManager.CreateAsync(adminUser4, "Admin4@325#");
            }
        }
    }
}