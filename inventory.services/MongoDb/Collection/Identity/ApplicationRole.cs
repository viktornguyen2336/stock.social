﻿using AspNetCore.Identity.MongoDbCore.Models;
using inventory.services.MongoDb.Common;

namespace inventory.services.MongoDb.Collection.Identity
{
    [CollectionName("Roles")]
    public class ApplicationRole : MongoIdentityRole<Guid>
    {
    }
}