﻿using AspNetCore.Identity.MongoDbCore.Models;
using inventory.services.MongoDb.Common;

namespace inventory.services.MongoDb.Collection.Identity
{
    [CollectionName("Users")]
    public class ApplicationUser : MongoIdentityUser<Guid>
    {
        public string Avatar { get; set; }
    }
}