import {createApp} from 'vue'
import App from "./App.vue";
import {createRouter, createWebHistory} from 'vue-router'
import store from './store'
import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";
import Home from "./Pages/Home.vue";
import PostCreate from "./Components/PostCreate.vue"
import PostDetail from "./Components/PostDetail.vue"
import OverviewSector from "./Pages/OverviewSector.vue";
import Enterprise from "./Pages/Enterprise.vue";
import MakePost from "./Pages/MakePost.vue";
import CreateSectorEnterprise from "./Pages/CreateSectorEnterprise.vue";
import Management from "./Pages/Management.vue";
import ManagementSector from "./Components/ManagementSector.vue";
import ManagementEnterprise from "./Components/ManagementEnterprise.vue";
import ManagementPost from "./Components/ManagementPost.vue";
import Chat from 'vue3-beautiful-chat'
import "./Resources/Styles/main.css";
import Feature from "./Pages/Feature.vue";

const routes = [
    {
        name: 'home',
        path: '/home',
        alias: '/',
        component: Home
    },
    {
        name: 'overview-sector',
        path: '/overview-sector/:id',
        component: OverviewSector
    },
    {
        name: 'content-management-edit',
        path: '/storage/content-management/edit',
        component: PostCreate
    },
    {
        name: 'post-detail',
        path: '/storage/post-detail/:id',
        component: PostDetail
    },
    {
        name: 'enterprise',
        path: '/enterprise/:id',
        component: Enterprise
    },
    {
        name: 'create-post',
        path: '/create-post',
        component: MakePost
    },
    {
        name: 'create-sector-enterprise',
        path: '/create-sector-enterprise',
        component: CreateSectorEnterprise
    },
    {
        name: 'story',
        path: '/story',
        component: Feature
    },
    {
        name: 'process',
        path: '/process',
        component: Feature
    },
    {
        name: 'q&a',
        path: '/q&a',
        component: Feature
    },
    {
        name: 'management',
        path: '/management',
        component: Management,
        children: [
            {
                path: 'sector',
                name: 'sector',
                component: ManagementSector
            },
            {
                path: 'enterprisem',
                name: 'enterprisem',
                component: ManagementEnterprise
            }, {
                path: 'post',
                name: 'post',
                component: ManagementPost
            }
        ]
    }

    //{
    //    path: `/${constant.urlsDefine.signIn}`,
    //    component: Login
    //},
    //{
    //    path: `/${constant.urlsDefine.signUp}`,
    //    component: SignUp
    //},
    //{
    //    path: `/${constant.urlsDefine.courseDetail}/:id`,
    //    component: CourseDetail
    //},
    //{
    //    path: `/${constant.urlsDefine.courseLearning}/:id`,
    //    component: MainLearning
    //},
    //{
    //    path: `/${constant.urlsDefine.dashboard}`,
    //    component: Dashboard,
    //    meta: {
    //        requiresAuth: true
    //    },
    //    children: [
    //        {
    //            path: `${constant.urlsDefine.createCourse}`,
    //            component: CreateCourse
    //        },
    //        {
    //            path: `${constant.urlsDefine.editCourse}/:id/:mode`,
    //            component: CreateCourse
    //        },
    //        {
    //            path: `${constant.urlsDefine.manageCourses}`,
    //            component: ManagementCourses,

    //        },
    //        {
    //            path: `${constant.urlsDefine.manageUsers}`,
    //            component: ManagementUsers
    //        }
    //    ]
    //}
]
const router = createRouter({
    history: createWebHistory(),
    routes: routes
});

router.beforeEach((to, from, next) => {
    //if (to.matched.some(record => record.meta?.requiresAuth)) {
    //    if (!store.state.isAuthenticated) {
    //        next({
    //            path: `/${constant.urlsDefine.signIn}`,
    //            query: {redirect: to.fullPath}
    //        })
    //    } else {
    //        next()
    //    }
    //} else {
    //    next()
    //}
    next();
})

const app = createApp(App)
app.use(router);
app.use(store);
app.use(Antd);
app.use(Chat);
app.mount('#app');
