﻿function MyCustomUploadAdapterPlugin(editor, imageManage) {
    if (!imageManage) {
        throw "imageManage must not undefine";
    }
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        return new MyUploadAdapter(loader, imageManage);
    };
}

class MyUploadAdapter {
    constructor(loader, imageManage) {

        this.loader = loader;
        this.imageManage = imageManage;
    }

    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest();

        xhr.open('POST', '/upload-temp-image', true);
        xhr.responseType = 'json';
        xhr.withCredentials = true;
    }

    _initListeners(resolve, reject, file) {
        const xhr = this.xhr;
        const loader = this.loader;
        const genericErrorText = `Couldn't upload file: ${file.name}.`;

        xhr.addEventListener('error', () => reject(genericErrorText));
        xhr.addEventListener('abort', () => reject());
        xhr.addEventListener('load', () => {
            const response = xhr.response;
            if (!response || response.error) {
                return reject(response && response.error ? response.error.message : genericErrorText);
            }
            if (!this.imageManage.images) {
                this.imageManage.images = [];
            }
            this.imageManage.images.push(response.url);
            resolve({
                default: response.url
            });
        });

        if (xhr.upload) {
            xhr.upload.addEventListener('progress', evt => {
                if (evt.lengthComputable) {
                    loader.uploadTotal = evt.total;
                    loader.uploaded = evt.loaded;
                }
            });
        }
    }

    upload() {
        return this.loader.file
            .then(file => new Promise((resolve, reject) => {
                this._initRequest();
                this._initListeners(resolve, reject, file);
                this._sendRequest(file);
            }));
    }

    // Aborts the upload process.
    abort() {
        if (this.xhr) {
            this.xhr.abort();
        }
    }

    _sendRequest(file) {
        // Prepare the form data.
        const data = new FormData();
        data.append('imageUrl', file);
        this.xhr.send(data);
    }
}

export default MyCustomUploadAdapterPlugin;