ClassicEditor
	.create(document.querySelector('.editor'), {
		toolbar: {
			items: [
				'heading','bold', 'italic',  'underline',
				'bulletedList', 'numberedList',
				'outdent', 'indent',
				'undo', 'redo',
				'-',
				'fontSize', 'fontFamily', 'fontColor', 'fontBackgroundColor', 'highlight',
				'link', 'insertImage', 'blockQuote', 'insertTable',
				 'horizontalLine', 
            ],
            shouldNotGroupWhenFull: true
		},
		heading: {
			options: [
				{ model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
				{ model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
				{ model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' },
				{ model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3' },
				{ model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4' },
				{ model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5' },
				{ model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6' }
			]
		},
		fontFamily: {
			options: [
				'default',
				'Arial, Helvetica, sans-serif',
				'Courier New, Courier, monospace',
				'Georgia, serif',
				'Lucida Sans Unicode, Lucida Grande, sans-serif',
				'Tahoma, Geneva, sans-serif',
				'Times New Roman, Times, serif',
				'Trebuchet MS, Helvetica, sans-serif',
				'Verdana, Geneva, sans-serif'
			],
			supportAllValues: true
		},
		fontSize: {
			options: [10, 12, 14, 'default', 18, 20, 22],
			supportAllValues: true
        },
        removePlugins: ["MediaEmbedToolbar"]
	} )
    .then(editor => {
        editor.plugins.get('FileRepository').createUploadAdapter = loader => {
            return new MyUploadAdapter(loader);
        };
		window.editor = editor;
		document.getElementById('btnSubmit').addEventListener('click', () => {
            const content = editor.getData(); // Lấy nội dung đã viết
            // Lưu content vào cơ sở dữ liệu của bạn (có thể dùng AJAX hoặc fetch API)
			console.log(content);
			document.getElementById('contentContainer').innerHTML = content;
        });
	} )
	.catch( handleSampleError );

// document.getElementById("btnSubmit").addEventListener('click', function () {
// 	const domEditableElement = document.querySelector('.ck-editor__editable_inline');
// 	const editorInstance = domEditableElement.ckeditorInstance;
// 	console.log(editorInstance.editor.getData());
// });


function handleSampleError( error ) {
	const issueUrl = 'https://github.com/ckeditor/ckeditor5/issues';

	const message = [
		'Oops, something went wrong!',
		`Please, report the following error on ${ issueUrl } with the build id "z6c6u62qvkte-nohdljl880ze" and the error stack trace:`
	].join( '\n' );

	console.error( message );
	console.error( error );
}

// function MyCustomUploadAdapterPlugin( editor ) {
//     editor.plugins.get('FileRepository').createUploadAdapter = function (loader) {
//         return new MyUploadAdapter(loader);
//     }
// }

class MyUploadAdapter {
    constructor( loader ) {
        // The file loader instance to use during the upload. It sounds scary but do not
        // worry — the loader will be passed into the adapter later on in this guide.
        this.loader = loader;
	}
	
    upload() {
        return "1144";
    }

    // Aborts the upload process.
}
