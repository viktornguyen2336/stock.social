const path = require('path');
const {VueLoaderPlugin} = require('vue-loader')
// vue-loader comes up with plugin. In a .vue file we can write both JS and CSS. This plugin ensures the Webpack rules specified for JS, CSS files are applied to that inside .vue files
let exportObject = {
    // entry: entries,
    entry: './index.js',
    output: {
        path: path.resolve(__dirname, '../wwwroot'),
        filename: 'js/[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
}
if (process.env.NODE_ENV === 'production') {
    exportObject.devtool = false;
    exportObject.mode = 'production';
} else {
    exportObject.devtool = 'eval-source-map';
}
module.exports = exportObject;