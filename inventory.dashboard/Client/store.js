﻿import {createStore} from "vuex";

const store = createStore({
    state: {
        // Trạng thái
        isLogin: false
    },
    mutations: {
        // Các hàm để thay đổi trạng thái
        changeLoginValue(state, isLogin) {
            state.isLogin = isLogin;
        },

    },
    actions: {},
    getters: {
        // Các hàm lấy dữ liệu từ trạng thái
    }
});

export default store