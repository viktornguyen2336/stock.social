﻿using inventory.dashboard.Models.AccountViewModels;
using inventory.services.MongoDb.Collection.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace inventory.dashboard.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AccountController(SignInManager<ApplicationUser> signInManager, UserManager<ApplicationUser> userManager)
        {
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, "User not found");
                    return View(model);
                }

                var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, true);
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        returnUrl = "/";
                    }

                    return Redirect(returnUrl);
                }

                ModelState.AddModelError(string.Empty, "Password inccorect");
            }

            return View(model);
        }

        [HttpGet("log-out")]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return Redirect("/home");
        }

        [HttpGet("get-user-info")]
        public async Task<IActionResult> GetUserInfo()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                return Ok(new
                {
                    UserName = user.UserName
                });
            }

            return BadRequest("user not authenticate yet");
        }
    }
}