﻿using System.Text;
using inventory.dashboard.Models;
using inventory.services.Model.Posts;
using inventory.services.MongoDb.Collection.Storage;
using inventory.services.MongoDb.Repository;
using inventory.services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace inventory.dashboard.Controllers
{
    [Authorize]
    public class PostController : ControllerBase
    {
        private readonly IS3Service _s3Service;
        private readonly IMongoRepository<Post> _postRepo;

        public PostController(IMongoRepository<Post> postRepo, IS3Service s3Service)
        {
            _postRepo = postRepo;
            _s3Service = s3Service;
        }

        [HttpGet("get-posts-by-enterprise-id/{id}")]
        public async Task<IActionResult> GetPostsByEnterpriseId(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("id must not null");
            }

            var posts = await _postRepo.GetAsync(x => x.Owner.OwnerId == id);
            return Ok(posts);
        }
        [HttpGet("get-posts-of-sectors")]
        [AllowAnonymous]
        public async Task<IActionResult> GetAllPostOfSectors()
        {
            var posts = await _postRepo.GetAsync(x => x.Owner.OwnerType == "sector");
            return Ok(posts);
        }

        [HttpPost("delete-post/{id}")]
        // [AllowAnonymous]
        public async Task<IActionResult> DeletePost(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("id must not null");
            }

            await _postRepo.DeleteAsync(id);
            return Ok();
        }

        [Authorize, HttpPost("upload-temp-image")]
        public async Task<IActionResult> UploadTempImage(UploadTempImageRequest request)
        {
            var path = await _s3Service.PushImageToS3WithExpired(request.ImageUrl.OpenReadStream(),
                request.ImageUrl.FileName);
            return Ok(new
            {
                Url = path
            });
        }

        [Authorize, HttpPost("create-post")]
        public async Task<IActionResult> CreatePost(CreatePostRequest request)
        {
            if (request.ImagesInContent != null && request.ImagesInContent.Any())
            {
                foreach (var path in request.ImagesInContent)
                {
                    await _s3Service.RemoveExpiredForImage(path);
                }
            }

            var post = new Post
            {
                Title = request.Title,
                Content = request.Content,
                PostType = request.PostType,
            };
            if (request.CardImage != null)
            {
                post.ImageUrl =
                    await _s3Service.PushImageToS3(request.CardImage.OpenReadStream(), request.CardImage.FileName);
            }


            if (!string.IsNullOrEmpty(request.SectorId))
            {
                post.Owner = new Owner
                {
                    OwnerType = "sector",
                    OwnerId = request.SectorId
                };
            }
            else
            {
                post.Owner = new Owner
                {
                    OwnerType = "enterprise",
                    OwnerId = request.EnterpriseId
                };
            }

            await _postRepo.AddAsync(post);
            return Ok(post);
        }
    }
}