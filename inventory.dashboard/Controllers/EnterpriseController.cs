﻿using inventory.dashboard.Models;
using inventory.services.MongoDb.Collection.Storage;
using inventory.services.MongoDb.Repository;
using inventory.services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace inventory.dashboard.Controllers;

public class EnterpriseController : ControllerBase
{
    private readonly IMongoRepository<Enterprise> _enterpriseRepo;
    private readonly IS3Service _s3Service;

    public EnterpriseController(IMongoRepository<Enterprise> enterpriseRepo, IS3Service s3Service)
    {
        _enterpriseRepo = enterpriseRepo;
        _s3Service = s3Service;
    }

    [HttpGet("get-all-enterprises")]
    public async Task<IActionResult> GetAllSector()
    {
        var enterprises = await _enterpriseRepo.GetAsync(x => true);
        return Ok(enterprises.Select(s => new
        {
            s.Id,
            s.Name,
            s.Code,
            s.ImageUrl,
            s.SectorId
        }));
    }

    [HttpGet("get-enterprises-by-sector-id/{id}")]
    public async Task<IActionResult> GetEnterpriseBySectorId(string id)
    {
        var enterprises = await _enterpriseRepo.GetAsync(x => x.SectorId == id);
        return Ok(enterprises.Select(s => new
        {
            s.Id,
            s.Name,
            s.Code,
            image_url = s.ImageUrl
        }));
    }

    [HttpGet("get-enterprise-detail/{id}")]
    public async Task<IActionResult> GetSector(string id)
    {
        var pipelines = new List<BsonDocument>
        {
            new BsonDocument("$match", new BsonDocument("$expr", new BsonDocument("$or", new BsonArray
            {
                new BsonDocument("$eq", new BsonArray { "$_id", id }),
                new BsonDocument("$eq", new BsonArray { "$code", id }),
                new BsonDocument("$eq", new BsonArray { "$name", id })
            }))),
            new BsonDocument("$lookup", new BsonDocument
            {
                { "from", "Post" },
                {
                    "let", new BsonDocument
                    {
                        { "enterprise_id", "$_id" },
                    }
                },
                {
                    "pipeline",
                    new BsonArray
                    {
                        new BsonDocument("$match",
                            new BsonDocument("$expr",
                                new BsonDocument("$and",
                                    new BsonArray
                                    {
                                        new BsonDocument("$eq", new BsonArray { "$owner.owner_type", "enterprise" }),
                                        new BsonDocument("$eq", new BsonArray { "$owner.owner_id", "$$enterprise_id" }),
                                    }))),
                        new BsonDocument("$project", new BsonDocument
                        {
                            { "post_type", 1 },
                            { "content", 1 },
                        })
                    }
                },
                { "as", "posts" }
            }),

            new BsonDocument("$project", new BsonDocument
            {
                { "_id", 0 },
                { "id", "$_id" },
                { "name", 1 },
                { "code", 1 },
                { "image_url", 1 },
                { "posts", 1 },
            })
        };
        var enterprise = await _enterpriseRepo.AggregateAsync<dynamic>(pipelines);
        return Ok(enterprise);
    }

    [Authorize, HttpPost("create-enterprise")]
    public async Task<IActionResult> CreateSector([FromForm] CreateEnterpriseRequest request)
    {
        var enterpriseFound = await _enterpriseRepo.GetAsync(s => s.SectorId == request.SectorId && (s.Name == request.Name || s.Code == request.Code));
        if (enterpriseFound != null && enterpriseFound.Any())
        {
            return BadRequest("Tên hoặc Mã của doanh nghiệp đã tồn tại");
        }

        var enterprise = new Enterprise()
        {
            Code = request.Code,
            Name = request.Name,
            SectorId = request.SectorId
        };
        if (request.ImageUrl != null)
        {
            using var stream = request.ImageUrl.OpenReadStream();
            enterprise.ImageUrl = await _s3Service.PushImageToS3(stream, request.ImageUrl.FileName, "enterprise");
        }

        await _enterpriseRepo.AddAsync(enterprise);
        return Ok(new
        {
            enterprise.Code,
            enterprise.Name,
            enterprise.Id
        });
    }

    [Authorize, HttpPost("delete-enterprise/{id}")]
    public async Task<IActionResult> CreateSector(string id)
    {
        if (string.IsNullOrEmpty(id))
        {
            return BadRequest("id must not null");
        }

        await _enterpriseRepo.DeleteAsync(id);
        return Ok();
    }
}