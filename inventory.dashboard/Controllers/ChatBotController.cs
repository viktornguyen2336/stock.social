﻿using System.Net;
using inventory.dashboard.Models;
using inventory.services.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace inventory.dashboard.Controllers;

public class ChatBotController : ControllerBase
{
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly ChatBotSetting _chatBotSetting;

    public ChatBotController(ChatBotSetting chatBotSetting, IHttpClientFactory httpClientFactory)
    {
        _chatBotSetting = chatBotSetting;
        _httpClientFactory = httpClientFactory;
    }

    [Authorize, HttpPost("api/chatbot/completion")]
    public async Task<IActionResult> Completion([FromBody] AskChatBotRequest request)
    {
        if (request.History == null || !request.History.Any())
        {
            return BadRequest();
        }

        var history = request.History.Select(x => new
        {
            role = "user",
            content = x
        }).ToArray();

        var httpClient = _httpClientFactory.CreateClient();
        var response = await httpClient.PostAsJsonAsync(_chatBotSetting.Url, new
        {
            apiKey = _chatBotSetting.SecretKey,
            sessionID = _chatBotSetting.SessionId,
            history = history,
            powerful = false,
            google = false
        });
        if (response.StatusCode == HttpStatusCode.OK)
        {
            var contentResponse = await response.Content.ReadFromJsonAsync<ChatBotResponse>();
            if (contentResponse.Success)
            {
                return Ok(contentResponse.Output);
            }
        }

        return Ok();
    }
}