﻿using inventory.dashboard.Models;
using inventory.services.MongoDb.Collection.Storage;
using inventory.services.MongoDb.Repository;
using inventory.services.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace inventory.dashboard.Controllers;

public class SectorController : ControllerBase
{
    private readonly IMongoRepository<Sector> _sectorRepo;
    private readonly IS3Service _s3Service;

    public SectorController(IMongoRepository<Sector> sectorRepo, IS3Service s3Service)
    {
        _sectorRepo = sectorRepo;
        _s3Service = s3Service;
    }

    [HttpGet("get-all-sectors")]
    public async Task<IActionResult> GetAllSector()
    {
        var sectors = await _sectorRepo.GetAsync(x => true);
        return Ok(sectors.Select(s => new
        {
            s.Id,
            s.Name,
            s.Code,
            s.ImageUrl
        }));
    }

    [HttpGet("get-sector-detail/{id}")]
    public async Task<IActionResult> GetSector(string id)
    {
        var pipelines = new List<BsonDocument>
        {
            new BsonDocument("$match", new BsonDocument("$expr", new BsonDocument("$or", new BsonArray
            {
                new BsonDocument("$eq", new BsonArray { "$_id", id }),
                new BsonDocument("$eq", new BsonArray { "$code", id }),
                new BsonDocument("$eq", new BsonArray { "$name", id })
            }))),
            new BsonDocument("$lookup", new BsonDocument
            {
                { "from", "Post" },
                {
                    "let", new BsonDocument
                    {
                        { "sector_id", "$_id" },
                    }
                },
                {
                    "pipeline",
                    new BsonArray
                    {
                        new BsonDocument("$match",
                            new BsonDocument("$expr",
                                new BsonDocument("$and",
                                    new BsonArray
                                    {
                                        new BsonDocument("$eq", new BsonArray { "$owner.owner_type", "sector" }),
                                        new BsonDocument("$eq", new BsonArray { "$owner.owner_id", "$$sector_id" }),
                                    }))),
                        new BsonDocument("$project", new BsonDocument
                        {
                            { "content", 1 },
                        })
                    }
                },
                { "as", "post_contents" }
            }),
            new BsonDocument("$lookup", new BsonDocument
            {
                { "from", "Enterprise" },
                {
                    "let", new BsonDocument
                    {
                        { "foreign_sector_id", "$_id" },
                    }
                },
                {
                    "pipeline",
                    new BsonArray
                    {
                        new BsonDocument("$match", new BsonDocument("$expr", new BsonDocument("$eq", new BsonArray
                        {
                            "$sector_id",
                            "$$foreign_sector_id",
                        }))),
                        new BsonDocument("$project", new BsonDocument
                        {
                            { "_id", 0 },
                            { "id", "$_id" },
                            { "code", 1 },
                            { "name", 1 }
                        })
                    }
                },
                { "as", "enterprises" }
            }),
            new BsonDocument("$project", new BsonDocument
            {
                { "_id", 0 },
                { "id", "$_id" },
                { "name", 1 },
                { "code", 1 },
                { "image_url", 1 },
                { "post_contents", 1 },
                { "enterprises", 1 }
            })
        };
        var sector = await _sectorRepo.AggregateAsync<dynamic>(pipelines);
        return Ok(sector);
    }

    [HttpGet("get-all-sectors-with-enterprises")]
    public async Task<IActionResult> GetAllSectorWithEnterPrise()
    {
        var pipelines = new List<BsonDocument>
        {
            new BsonDocument("$lookup", new BsonDocument
            {
                { "from", "Enterprise" },
                { "localField", "_id" },
                { "foreignField", "sector_id" },
                { "as", "results" }
            }),
            new BsonDocument("$project", new BsonDocument
            {
                { "_id", 0 },
                { "id", "$_id" },
                { "name", 1 },
                { "code", 1 },
                { "image_url", 1 },
                { "overview_post_id", 1 },
                {
                    "enterprises", new BsonDocument("$map", new BsonDocument
                    {
                        { "input", "$results" },
                        { "as", "item" },
                        {
                            "in", new BsonDocument
                            {
                                { "id", "$$item._id" },
                                { "name", "$$item.name" },
                                { "code", "$$item.code" },
                            }
                        }
                    })
                }
            })
        };

        var sectors = await _sectorRepo.AggregateAsync<dynamic>(pipelines);
        return Ok(sectors);
    }

    [Authorize, HttpPost("create-sector")]
    public async Task<IActionResult> CreateSector([FromForm] CreateSectorRequest request)
    {
        var sectorSameName = await _sectorRepo.GetAsync(s => s.Name == request.Name);
        if (sectorSameName != null && sectorSameName.Any())
        {
            return BadRequest("Tên hoặc Mã của sector đã tồn tại");
        }

        using var stream = request.ImageUrl.OpenReadStream();
        var path = await _s3Service.PushImageToS3(stream, request.ImageUrl.FileName, "sector");

        var sector = new Sector()
        {
            Code = request.Code,
            Name = request.Name,
            ImageUrl = path
        };

        await _sectorRepo.AddAsync(sector);
        return Ok(new
        {
            sector.Code,
            sector.Name,
            sector.Id
        });
    }
    
    // [HttpPost("create-sector-test")]
    // public async Task<IActionResult> CreateSectorTest([FromBody] CreateSectorTest request)
    // {
    //     var sector = new Sector();
    //     sector.Id = request.Id;
    //     sector.ImageUrl = request.Image;
    //     sector.Name = request.Name;
    //     await _sectorRepo.AddAsync(sector);
    //     return Ok(new
    //     {
    //         sector.Code,
    //         sector.Name,
    //         sector.Id
    //     });
    // }

    // [Authorize, HttpPost("update-sector")]
    // public async Task<IActionResult> UpdateSector([FromForm] CreateSectorRequest request)
    // {
    //     var sectorSameName = await _sectorRepo.GetAsync(s => s.Name == request.Name);
    //     if (sectorSameName != null && sectorSameName.Any())
    //     {
    //         return BadRequest("Tên hoặc Mã của sector đã tồn tại");
    //     }
    //
    //     using var stream = request.ImageUrl.OpenReadStream();
    //     var path = await _s3Service.PushImageToS3(stream, request.ImageUrl.FileName, "sector");
    //
    //     var sector = new Sector()
    //     {
    //         Code = request.Code,
    //         Name = request.Name,
    //         ImageUrl = path
    //     };
    //
    //     await _sectorRepo.AddAsync(sector);
    //     return Ok(new
    //     {
    //         sector.Code,
    //         sector.Name,
    //         sector.Id
    //     });
    // }

    [Authorize, HttpPost("delete-sector/{id}")]
    public async Task<IActionResult> UpdateSector(string id)
    {
        if (string.IsNullOrEmpty(id))
        {
            return BadRequest("id must not null");
        }

        await _sectorRepo.DeleteAsync(id);
        return Ok();
    }
}