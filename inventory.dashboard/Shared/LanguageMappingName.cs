namespace inventory.dashboard.Shared;

public class LanguageMappingName
{
    public static readonly IDictionary<string, string> languageMappingNameDictionary = new Dictionary<string, string>
    {
        {"ar-SA", ""},
        {"zh-CN", ""},
        {"zh-TW", ""},
        {"hr-HR", ""},
        {"cs-CZ", ""},
        {"da-DK", ""},
        {"en-US", ""},
        {"fi-FI", ""},
        {"fr-FR", ""},
        {"fr-CA", ""},
        {"de-DE", ""},
        {"el-GR", ""},
        {"hu-HU", ""},
        {"is-IS", ""},
        {"id-ID", ""},
        {"it-IT", ""},
        {"ja-JP", ""},
        {"ko-KR", ""},
        {"lt-LT", ""},
        {"ms-MY", ""},
        {"nb-NO", ""},
        {"pl-PL", ""},
        {"pt-BR", ""},
        {"pt-PT", ""},
        {"ru-RU", ""},
        {"sk-SK", ""},
        {"es-MX", ""},
        {"es-ES", ""},
        {"sv-SE", ""},
        {"th-TH", ""},
        {"tr-TR", ""},
        {"uk-UA", ""},
        {"vi-VN", ""}
    };
}