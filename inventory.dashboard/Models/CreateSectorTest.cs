﻿namespace inventory.dashboard.Models;

public class CreateSectorTest
{
    public string Id { get; set; }

    public string Image { get; set; }

    public string Name { get; set; }

    public bool IsUpdate { get; set; }
}