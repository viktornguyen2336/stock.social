﻿using System.ComponentModel.DataAnnotations;

namespace inventory.dashboard.Models;

public class CreateSectorRequest
{
    [Required]
    public string Id { get; set; }
    
    [Required]
    public string Name { get; set; }
    public string Code { get; set; }

    [Required]
    public IFormFile ImageUrl { get; set; }
}