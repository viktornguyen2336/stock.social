﻿namespace inventory.dashboard.Models;

public class CreatePostRequest
{
    public string Title { get; set; }

    public string SectorId { get; set; }

    public string EnterpriseId { get; set; }

    public string PostType { get; set; }

    public IFormFile CardImage { get; set; }

    public string[] ImagesInContent { get; set; }

    public string Content { get; set; }
}