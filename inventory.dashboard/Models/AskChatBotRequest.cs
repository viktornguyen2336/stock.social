﻿namespace inventory.dashboard.Models;

public class AskChatBotRequest
{
    public string[] History { get; set; }
}