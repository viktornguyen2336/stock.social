﻿namespace inventory.dashboard.Models;

public class ChatBotResponse
{
    public bool Success { get; set; }

    public Output Output { get; set; }
}

public class Output
{
    public string Completion { get; set; }
}