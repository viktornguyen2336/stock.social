﻿using System.ComponentModel.DataAnnotations;

namespace inventory.dashboard.Models;

public class UploadTempImageRequest
{
    [Required]
    public IFormFile ImageUrl { get; set; }
}