﻿using System.ComponentModel.DataAnnotations;

namespace inventory.dashboard.Models;

public class CreateEnterpriseRequest
{
    [Required]
    public string Name { get; set; }

    [Required]
    public string Code { get; set; }

    [Required]
    public string SectorId { get; set; }
    
    public IFormFile ImageUrl { get; set; }
}