using inventory.services;
using inventory.services.Model;
using inventory.services.MongoDb;
using inventory.services.MongoDb.Collection.Identity;
using inventory.services.MongoDb.Collection.Storage;
using Microsoft.AspNetCore.Identity;
using MongoDbGenericRepository;

var builder = WebApplication.CreateBuilder(args);

var mongoDbSetting = builder.Configuration.GetSection(nameof(MongoDbSetting)).Get<MongoDbSetting>();
// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var chatbotSetting = builder.Configuration.GetSection("ChatBotSetting").Get<ChatBotSetting>();
builder.Services.AddSingleton(chatbotSetting);
var mongoClient = builder.Services.RegisterMongoDb(mongoDbSetting);
builder.Services.AddIdentityCore<ApplicationUser>()
    .AddRoles<ApplicationRole>()
    .AddMongoDbStores<IMongoDbContext>(new MongoDbContext(mongoClient, mongoDbSetting.IdentityDatabase))
    .AddSignInManager()
    .AddDefaultTokenProviders();

builder.Services.AddAuthentication(o =>
    {
        o.DefaultScheme = IdentityConstants.ApplicationScheme;
        o.DefaultSignInScheme = IdentityConstants.ExternalScheme;
    })
    .AddIdentityCookies(o => { });

builder.Services.AddHttpClient();

builder.Services.AddTransient<SeedData>();

//builder.Services.AddAuthorization(options =>
//{
//    options.AddPolicy("RequiredAdmin", policyBuilder =>
//    {
//        policyBuilder.AddRequirements(new RoleRequirement("Admin"));
//    });
//});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();

app.UseAuthorization();

app.Map("/health", app => { app.Run(async context => { await context.Response.WriteAsync("Ok"); }); });

app.MapDefaultControllerRoute();

app.MapFallbackToController("Index", "Home");

// seed account admin
using var scope = app.Services.CreateScope();
scope.ServiceProvider.GetService<SeedData>().SeedAdminAsync().Wait();

app.Run();